# -*- coding: utf-8 -*-
import os
import shutil
import unicodedata
from string import upper, strip

# CSV file encoding
CSV_ENCODING = 'cp1250'

# define inputs
file_with_dir_list = "/Users/sevrjukov/Work/Git/dirclean/resources/stav_zbozi.csv"
source_directory = "/Users/sevrjukov/Work/Git/dirclean/resources/vzor_export"
archive_directory = "/tmp/archive"


# FUNCTION DEFINITIONS

# Strips diacritics, replaces spaces with underscores
def normalize(text):
    try:
        text = unicode(text, CSV_ENCODING)
    except NameError:
        pass
    text = unicodedata.normalize('NFD', text) \
        .encode('ascii', 'ignore') \
        .decode(CSV_ENCODING)
    result = str(text)
    result = result.replace(" ", "_")
    return result


# reads file content, returns list of all lines
def readFileContent(filename):
    with open(filename) as f:
        content = f.readlines()
    return content


# Parses CSV file, returns dictionary object
# with records in format  dir_name = should_delete
# for example "CBLACRAcer TravelMate 4600" => True
def parse_list_of_dirs(filename):
    content = readFileContent(filename)
    dirs_list = {}
    first_line = True
    for line in content:
        # skip CSV header
        if first_line:
            first_line = False
            continue
        # strip whitespace chars
        line = line.strip()
        # skip empty lines
        if len(line) == 0:
            continue
        tokens = line.split(";")
        should_delete = (tokens[2] == 'NULL' or (float(tokens[2]) == 0.0))
        dirs_list[normalize(tokens[0])] = should_delete
    return dirs_list


# ACTUAL PROGRAM

# prepare list of dirs to be archived
dirs_list_from_file = parse_list_of_dirs(file_with_dir_list)

# create empty list
list_of_dirs_to_archive = []
list_of_unknown_dirs = []

# walk trough all files in the directory
for root, dirs, files in os.walk(source_directory):
    for dir_name in dirs:
        if dirs_list_from_file.keys().__contains__(dir_name):
            if dirs_list_from_file[dir_name]:
                print "This dir will be archived: [" + dir_name + "]"
                list_of_dirs_to_archive.append(dir_name)
            else:
                print "Product still in use, dir will NOT be archived [" + dir_name + "]"
        else:
            list_of_unknown_dirs.append(dir_name)
            print "WARNING this directory not found in list: [" + dir_name + "]"

print ""
print "Totally " + str(len(list_of_dirs_to_archive)) + " dirs to be archived."
if len(list_of_unknown_dirs) > 0:
    print "WARNING There are " + str(len(list_of_unknown_dirs)) + " dirs not listed in the export file."

confirm = raw_input("Do you want to continue? Y/N\n")

if upper(confirm) == 'Y':
    for dir_name in list_of_dirs_to_archive:
        print "Archiving directory " + dir_name
        try:
            shutil.move(os.path.join(source_directory, dir_name), archive_directory)
        except Exception:
            print "Failed to archive " + dir_name
else:
    print "Operation canceled"
